Rails.application.routes.draw do
  devise_for :users
  resources :users, only: :show
  
  get 'introduction/:id', to: 'users#intro', as: :introduction
  patch 'users/:id', to: 'users#update'

  resources :pins do
    member do
      put "like", to: "pins#like"
    end
  end
  root "pins#index"
end
