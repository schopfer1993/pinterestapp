class PinsController < ApplicationController
  before_action :find_pin, only: [:show, :edit, :update, :destroy, :like ]
  before_action :authenticate_user!, except: [:index, :show]
  before_action :correct_user, only: [:edit, :update, :destroy]

  def index
    @pins = Pin.all.order("created_at DESC").includes(:user)
  end

  def show
  end

  def edit
  end

  def update
    if @pin.update(pin_params)
      redirect_to @pin, notice: "更新成功！"
    else
      render 'edit'
    end
  end

  def destroy
    @pin.destroy
    redirect_to root_path, notice: "刪除成功"
  end

  def new
    @pin = current_user.pins.build
  end
  def create
    @pin = current_user.pins.build(pin_params)
    if @pin.save
      redirect_to @pin, notice: "發文成功！"
    else
      render 'new'
    end
  end

  def like
    @pin.liked_by(current_user)
    redirect_to :back
  end

  private
    def pin_params
      params.require(:pin).permit(:title, :description, :image)
    end
    def find_pin
      @pin = Pin.find(params[:id])
    end
    def correct_user
      redirect_to root_path, notice: "你沒有權限!" unless @pin.user == current_user
    end
end
