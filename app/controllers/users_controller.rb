class UsersController < ApplicationController
  before_action :authenticate_user!, only: :intro
  before_action :correct_user, only: :intro
  before_action :find_user, only: [:show, :update, :intro, :update]
  def show
  end
  def intro
  end
  def update
    if @user.update(params_user)
      redirect_to @user, notice: "修改成功！"
    else
      render 'intro'
    end
  end
  private
    def correct_user
      @user = User.find(params[:id])
      redirect_to root_url, notice: "你沒有權限!" unless @user == current_user
    end

    def params_user
      params.require(:user).permit(:introduction)
    end

    def find_user
      @user = User.find(params[:id])
    end
end
